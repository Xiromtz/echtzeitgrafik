#pragma once

#include "NVAPI/nvapi.h"
#include "NVAPI/NvApiDriverSettings.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>

class DriverAPI
{
private:
  int aaIndex = 0;
  NvDRSSessionHandle hSession;
  NvDRSProfileHandle hProfile;
  NvU32 antialiasingModeID;
  NVDRS_SETTING antiAliasingSetting;
  std::vector<EValues_AA_MODE_METHOD> aaMethods;

  void PrintError(NvAPI_Status status);
public:
  DriverAPI();
  ~DriverAPI();

  void setNextAAMethod();
};

