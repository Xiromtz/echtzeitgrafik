#pragma once
#include "GLShader.h"
#include "Floor.h"
#include "Camera.h"
#include "GLObject.h"
#include "Curve.h"
#include "DepthMap.h"
#include "Model.h"
#include <unordered_map>
#include "KD_Tree.h"
#include "Cube.h"

class Renderer
{
private:
    KD_Tree kdTree;
    KD_Tree::KDNode* root;
	GLShader* shadowShader;
	GLShader* greenShader;
	DepthMap* depthMap;
	Floor* floor;
    glm::mat4 projection;
	glm::mat4 lightMatrix;
	glm::mat4 lightProjection;
	glm::vec3 lightPos;
	glm::vec3 otherLightPos;
	glm::vec3 mixedLightPos;
	float lightPosMix = 0.0f;
	bool useLighting = true;
	float currentBumpiness = 1.0f;
	glm::vec3 lightUp;
	glm::vec3 lightLookAt;
	void setup();
	
public:
	std::unordered_map<GLShader*, std::vector<GLObject*>> objects;
	Curve* curve;
	Camera* camera;
	Renderer();
	~Renderer();
	void render(GLFWwindow* window);
	void renderDepthMap() const;
	void renderShadowObjects();
	void renderGreenObjects();
	inline void toggleLighting()
	{
		useLighting = !useLighting;
	}
	void addBumpiness()
	{
		if (currentBumpiness < 1.0f)
		{
			currentBumpiness += 0.01f;
      shadowShader->use();
			shadowShader->setFloat("normalIntensity", currentBumpiness);
		}
	}

	void removeBumpiness()
	{
		if (currentBumpiness > 0.0f)
		{
			currentBumpiness -= 0.01f;
      shadowShader->use();
			shadowShader->setFloat("normalIntensity", currentBumpiness);
		}
	}

	void moveLightRight()
	{
		if (lightPosMix < 1.0f)
		{
			lightPosMix += 0.01f;
			if (lightPosMix > 1.0f)
				lightPosMix = 1.0f;
		}
	}

	void moveLightLeft()
	{
		if (lightPosMix > 0.0f)
		{
			lightPosMix -= 0.01f;
			if (lightPosMix < 0.0f)
				lightPosMix = 0.0f;
		}
	}
};

