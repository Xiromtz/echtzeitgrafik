#include "Curve.h"

Curve::Curve()
{
	interpolatedPoints = getInterpolatedPositions(0, 1, 0.01f, 0.0f, interpolatedPoints);

	interpolatedVertexCount = interpolatedPoints.size() * 3;
	interpolatedVertices = new float[interpolatedVertexCount];

	unsigned int u = 0;
	for (int i = 0; i < interpolatedPoints.size(); i++)
	{
		interpolatedVertices[u] = interpolatedPoints[i].x;
		interpolatedVertices[u + 1] = interpolatedPoints[i].y;
		interpolatedVertices[u + 2] = interpolatedPoints[i].z;
		u += 3;
	}

	interpolatedIndexSize = interpolatedPoints.size() * 2 - 2;
	interpolatedIndices = new unsigned int[interpolatedIndexSize]();

	u = 0;
	for (unsigned int i = 0; i < interpolatedPoints.size() - 1; i++)
	{
		interpolatedIndices[u] = i;
		interpolatedIndices[u + 1] = i + 1;
		u += 2;
	}

	vAttrs = new VertexAttributes(interpolatedVertices, interpolatedVertexCount * sizeof(float), interpolatedIndices,
	                              interpolatedIndexSize * sizeof(unsigned int));
	vAttrs->set_coordinate_attributes();
	init();

	for (int i = 0; i < 10; i++)
	{
		SimpleCube* waypoint = new SimpleCube();
		waypoint->translate(wayPoints[i]);
		waypoint->rotate(curveAngles[i]);
		waypoint->scale(glm::vec3(0.05f, 0.05f, 0.05f));
        waypoint->update();
        waypoint->setTriangles();
		wayPointCubes.push_back(waypoint);
	}
    setTriangles();
}

Curve::~Curve()
{
	delete[] interpolatedWayPoints;
	for (int i = 0; i < 10; i++)
	{
		delete wayPointCubes[i];
	}
}

void Curve::update()
{
	GLObject::update();
	for (int i = 0; i < 10; i++)
	{
		wayPointCubes[i]->update();
	}
}

void Curve::setTriangles()
{
    for (auto it = wayPointCubes.begin(); it != wayPointCubes.end(); ++it)
    {
        std::vector<Triangle*> cubeTriangles = (*it)->getTriangles();
        triangles.insert(triangles.end(), cubeTriangles.begin(), cubeTriangles.end());
    }
}


std::vector<Triangle*> Curve::getTriangles() const
{    
    return triangles;
}


void Curve::draw(const GLShader& shader) const
{
	for (int i = 0; i < 10; i++)
	{
		wayPointCubes[i]->draw(shader);
	}
	vao->bind();
	shader.setMatrix("model", model);
	glDrawElements(GL_LINE_STRIP, interpolatedIndexSize, GL_UNSIGNED_INT, interpolatedIndices);
	vao->unbind();
}

std::vector<glm::vec3> Curve::getInterpolatedPositions(const unsigned index1, const unsigned index2, const float step,
                                                       float alpha, std::vector<glm::vec3> curve)
{
	if (alpha > 1)
		alpha = 1;

	glm::vec3 p = interpolatePosition(index1, index2, alpha);
	curve.push_back(p);

	if (alpha >= 1)
	{
		if (index2 == wayPointCount - 1)
		{
			return curve;
		}
		return getInterpolatedPositions(index1 + 1, index2 + 1, step, step, curve);
	}
	return getInterpolatedPositions(index1, index2, step, alpha + step, curve);
}

glm::vec3 Curve::interpolatePosition(const unsigned int index1, const unsigned int index2, const float percent) const
{
	glm::vec3 p1 = wayPoints[index1];
	glm::vec3 p2 = wayPoints[index2];

	glm::vec3 p0 = wayPoints[index1 == 0 ? index1 : index1 - 1];
	glm::vec3 p3 = wayPoints[index2 == (wayPointCount - 1) ? index2 : index2 + 1];

	glm::vec3 tang1 = 0.5f * ((p1 - p0) + (p2 - p1));
	glm::vec3 tang2 = 0.5f * ((p2 - p1) + (p3 - p2));

	glm::vec3 p = ((2 * std::pow(percent, 3)) - (3 * std::pow(percent, 2)) + 1) * p1
		+ (std::pow(percent, 3) - (2 * std::pow(percent, 2)) + percent) * tang1
		+ (std::pow(percent, 3) - std::pow(percent, 2)) * tang2
		+ ((-2 * std::pow(percent, 3)) + (3 * std::pow(percent, 2))) * p2;
	return p;
}



std::vector<glm::quat> Curve::getInterpolatedRotations(const unsigned index1, const unsigned index2, const float step, float alpha, std::vector<glm::quat> rotations)
{
	if (alpha > 1)
		alpha = 1;

	glm::quat q = interpolateRotation(index1, index2, alpha);
	rotations.push_back(q);

	if (alpha >= 1)
	{
		if (index2 == wayPointCount - 1)
		{
			return rotations;
		}
		return getInterpolatedRotations(index1 + 1, index2 + 1, step, step, rotations);
	}
	return getInterpolatedRotations(index1, index2, step, alpha + step, rotations);
}


glm::quat Curve::interpolateRotation(const unsigned index1, const unsigned index2, const float percent) const
{
	glm::quat q1 = glm::quat(curveAngles[index1]);
	glm::quat q2 = glm::quat(curveAngles[index2]);

	glm::quat q0 = glm::quat(curveAngles[index1 == 0 ? index1 : index1 - 1]);
	glm::quat q3 = glm::quat(curveAngles[index2 == (wayPointCount - 1) ? index2 : index2 + 1]);

	glm::quat qa = intermediate(q0, q1, q2);
	glm::quat qb = intermediate(q1, q2, q3);

	return glm::normalize(squad(q1, qa, qb, q2, percent));
}


glm::quat Curve::squad(glm::quat q1, glm::quat t1, glm::quat t2, glm::quat q2, const float percent) const
{
	float slerpT = 2.0f * percent * (1.0f - percent);
	glm::quat slerp1 = slerp(q1, q2, percent);
	glm::quat slerp2 = slerp(t1, t2, percent);
	return slerp(slerp1, slerp2, slerpT);
}

glm::quat Curve::intermediate(glm::quat q0, glm::quat q1, glm::quat q2) const
{
	glm::quat q1Inv = glm::inverse(q1);
	glm::quat c1 = q1Inv * q2;
	glm::quat c2 = q1Inv * q0;

	c1 = logQuat(c1);
	c2 = logQuat(c2);

	glm::quat c3 = c2 + c1;
	c3 = scaleQuat(c3, -0.25f);
	c3 = expQuat(c3);
	glm::quat r = q1 * c3;
	r = normalize(r);
	return r;
}

glm::quat Curve::logQuat(glm::quat q) const
{
	float a0 = q.w;
	q.w = 0.0f;
	if (glm::abs(a0) < 1.0f)
	{
		float angle = glm::acos(a0);
		float sinAngle = glm::sin(angle);
		if (glm::abs(sinAngle) >= 1.0e-15)
		{
			float coeff = angle / sinAngle;
			q.x *= coeff;
			q.y *= coeff;
			q.z *= coeff;
		}
	}
	return q;
}

glm::quat Curve::scaleQuat(glm::quat q, float scale) const
{
	q.w *= scale;
	q.x *= scale;
	q.y *= scale;
	q.z *= scale;
	return q;
}

glm::quat Curve::expQuat(glm::quat q) const
{
	float angle = glm::sqrt(q.x*q.x + q.y*q.y + q.z * q.z);
	float sinAngle = glm::sin(angle);
	q.w = glm::cos(angle);
	if (glm::abs(sinAngle) >= 1.0e-15)
	{
		float coeff = sinAngle / angle;
		q.x *= coeff;
		q.y *= coeff;
		q.z *= coeff;
	}
	return q;
}


glm::quat Curve::slerp(glm::quat q1, glm::quat q2, const float percent) const
{
	float cosAngle = q1.x * q2.x + q1.y * q2.y + q1.z * q2.z;
	float angle = acos(cosAngle);

	float wp = (sin(1 - percent) * angle) / (sin(angle));
	float wq = (sin(percent) * angle) / (sin(angle));

	glm::quat q = wp * q1 + wq * q2;
	return q;
}