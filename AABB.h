#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "Triangle.h"

class AABB : public GLObject
{
private:
    float vertices[72];
    unsigned int indices[36] =
    {
        0,1,2,2,3,0,
        4,5,6,6,7,4,
        8,9,10,10,11,8,
        12,13,14,14,15,12,
        16,17,18,18,19,16,
        20,21,22,22,23,20
    };
    void calculateCenterExtents();
public:
    glm::vec3 min;
    glm::vec3 max;

    glm::vec3 center;
    float extents[3];

    explicit AABB(glm::vec3 min, glm::vec3 max);
    explicit AABB(const std::vector<Triangle*>& triangles);

    ~AABB();

    void draw(const GLShader& shader) const override;
    std::vector<Triangle*> getTriangles() const override { return std::vector<Triangle*>(); }
    void calcVertices();
    void splitAABB(float val, int axis, AABB& front, AABB& back) const;
    int TestTriangle(const Triangle& triangle) const;
    int TestPlane(glm::vec3 normal, float dist) const;
};