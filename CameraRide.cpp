#include "CameraRide.h"
#include <GLFW/glfw3.h>


CameraRide::CameraRide(Camera* camera, const Curve* curve) : camera(camera), curve(curve)
{
	startIndex = 0;
	currentIndex = 0;
	endIndex = curve->wayPointCount-1;
}

CameraRide::~CameraRide()
{
}

void CameraRide::update()
{
	if (alpha == 1)
	{
		alpha = 0;
		currentIndex++;
		recalculate = true;
	}
	
	if (currentIndex == endIndex)
		return;
	
	if (recalculate)
	{
		startTime = glfwGetTime();
		currentDistance = distance(currentIndex, currentIndex + 1);
		
		recalculate = false;
	}

	time = currentDistance / velocity;

	deltaTime = glfwGetTime() - startTime;

	alpha = deltaTime/time;
	if (alpha > 1)
		alpha = 1;

	glm::vec3 pos = curve->interpolatePosition(currentIndex, currentIndex + 1, alpha);
	glm::quat rot = curve->interpolateRotation(currentIndex, currentIndex + 1, alpha);

	camera->setPos(pos);
	camera->rotate(rot);
}


float CameraRide::distance(unsigned startIndex, unsigned endIndex) const
{
	glm::vec3 p1 = curve->wayPoints[startIndex];
	glm::vec3 p2 = curve->wayPoints[endIndex];
	return glm::distance(p1, p2);
}
