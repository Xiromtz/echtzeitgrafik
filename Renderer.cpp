#include "Renderer.h"
#include <GLFW/glfw3.h>

Renderer::Renderer()
{
	shadowShader = new GLShader("Shaders/Shadow.vert", "Shaders/Shadow.frag");
	greenShader = new GLShader("Shaders/Color.vert", "Shaders/Color.frag");
	camera = new Camera();
	setup();
}

void Renderer::setup()
{
    /*Model* chapel = new Model("Obj/chapel.obj");
    objects[shadowShader].push_back(chapel);*/

	floor = new Floor();
	floor->translate(glm::vec3(0.0f, -3.5f, -10.0f));
    floor->update();
    floor->setTriangles();
	//floor->scale(glm::vec3(1.5f, 1.5f, 1.5f));
	objects[shadowShader].push_back(floor);

	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  1.0f,  -7.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -20.5f),
	};

	for (int i = 0; i < 3; i++)
	{
		std::string path = "Texture/cube" + std::to_string(i + 1) + ".png";
		std::string normalPath = "Texture/cube" + std::to_string(i + 1) + "_normal.png";
		Cube* cube = new Cube(path.c_str(), normalPath.c_str());
		cube->translate(cubePositions[i]);
		cube->rotate(20.0f * i, glm::vec3(1.0f, 0.3f, 0.5f));
        cube->update();
        cube->setTriangles();
		objects[shadowShader].push_back(cube);
	}

	//curve = new Curve();
	//objects[greenShader].push_back(curve);

    std::vector<Triangle*> allTriangles;
    for (auto it = objects.begin(); it != objects.end(); ++it)
    {
        std::vector<GLObject*> current = it->second;
        for (int i = 0; i < current.size(); i++)
        {
            std::vector<Triangle*> currentTriangles = current[i]->getTriangles();
            allTriangles.insert(allTriangles.begin(), currentTriangles.begin(), currentTriangles.end());
        }
    }

    std::vector<Triangle*> tempTriangles(allTriangles);
    AABB* first = new AABB(allTriangles);
    objects[greenShader].push_back(first);
    root = kdTree.BuildTree(tempTriangles, *first, 0, FLT_MAX);
    std::cout << kdTree.maxDepth << std::endl;
	
	lightPos = glm::vec3(1.0f, 20.0f, -10.0f);
	otherLightPos = glm::vec3(-5.0f, 20.0f, 10.0f);

	mixedLightPos = glm::mix(lightPos, otherLightPos, lightPosMix);

	lightUp = glm::vec3(0.0f, 1.0f, 0.0f);
	lightProjection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, 1.0f, 50.0f);
	glm::mat4 lightView = glm::lookAt(mixedLightPos,
		glm::vec3(0,0,0),
		lightUp);
	lightMatrix = lightProjection * lightView;

	projection = glm::mat4(1.0f);
	projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);
	depthMap = new DepthMap();

	shadowShader->use();
	shadowShader->setInt("diffuseTexture", 0);
	shadowShader->setInt("shadowMap", 1);
	shadowShader->setInt("normalMap", 2);
	shadowShader->setFloat("normalIntensity", 1.0f);
	shadowShader->setMatrix("projection", projection);

	greenShader->use();
	greenShader->setMatrix("projection", projection);
}

Renderer::~Renderer()
{
	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		std::vector<GLObject*> current = it->second;
		for (unsigned int i = 0; i < current.size(); i++)
		{
			delete current[i];
		}
	}
	delete shadowShader;
	delete greenShader;
	delete camera;
	delete depthMap;
}

void Renderer::render(GLFWwindow* window)
{	
 	mixedLightPos = glm::mix(lightPos, otherLightPos, lightPosMix);
	glm::mat4 lightView = glm::lookAt(mixedLightPos,
		glm::vec3(0, 0, 0),
		lightUp);
	lightMatrix = lightProjection * lightView;

	renderDepthMap();

	glViewport(0, 0, 800, 600);
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderShadowObjects();
	renderGreenObjects();

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Renderer::renderDepthMap() const
{
	depthMap->bind();
	depthMap->shader->setMatrix("lightSpaceMatrix", lightMatrix);

	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		std::vector<GLObject*> current = it->second;
		for (int i = 0; i < current.size(); i++)
			current[i]->draw(*depthMap->shader);
	}

	depthMap->unbind();
}

void Renderer::renderShadowObjects()
{
	shadowShader->use();

	depthMap->depthMap->bind();
	camera->draw(*shadowShader);
	shadowShader->setVec3("viewPos", camera->getPos());
	shadowShader->setVec3("lightPos", mixedLightPos);
	shadowShader->setMatrix("lightSpaceMatrix", lightMatrix);
	shadowShader->setBool("useLighting", useLighting);

	std::vector<GLObject*> shadowObjects = objects[shadowShader];
	for (unsigned int i = 0; i < shadowObjects.size(); i++)
	{
		shadowObjects[i]->draw(*shadowShader);
	}
}

void Renderer::renderGreenObjects()
{
	greenShader->use();
	camera->draw(*greenShader);
	std::vector<GLObject*> greenObjects = objects[greenShader];
	for (unsigned int i = 0; i < greenObjects.size(); i++)
	{
		greenObjects[i]->draw(*greenShader);
	}

    /*for (auto it = objects.begin(); it != objects.end(); ++it)
    {
        std::vector<GLObject*> current = it->second;
        for (int i = 0; i < current.size(); i++)
        {
            std::vector<Triangle*> triangles = current[i]->getTriangles();
            for (int u = 0; u < triangles.size(); u++)
            {
                triangles[u]->draw(*greenShader);
            }
        }
    }*/

    //kdTree.draw(*greenShader);
    glm::vec3 cameraPos, lookAt;
    float tMax = 0;
    camera->setLookAtVals(cameraPos, lookAt, tMax, projection);
    Triangle* intersection = kdTree.VisitNodes(root, cameraPos, lookAt, tMax);
    if (intersection)
        intersection->draw(*greenShader);
}

