#pragma once
#include <glm/glm.hpp>
#include "GLObject.h"

class Triangle : public GLObject
{
private:
    VertexAttributes* centerOfMassVertexAttrs = nullptr;
    float vertices[9];

    VBO* centerOfMassVBO;
    VAO* centerOfMassVAO;

    float centerOfMassVertices[6];
    unsigned int indices[3] = { 0, 1, 2 };
    unsigned int centerOfMassIndices[2] = { 0, 1 };
public:
    glm::vec3 centerOfMass;
    glm::vec3 normal;
    glm::vec3 points[3];

    bool ignore = false;

    explicit Triangle(){}
    explicit Triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
    ~Triangle();
    glm::vec3 calculateCenterOfMass() const;
    glm::vec3 calculateNormal() const;

    void setup();
    void createCenterOfMassVertices();
    void createVertices();
    void draw(const GLShader& shader) const override;
    std::vector<Triangle*> getTriangles() const override { return std::vector<Triangle*>(); }
    float intersectSegment(glm::vec3 a, glm::vec3 d) const;

    friend bool operator==(const Triangle& lhs, const Triangle& rhs)
    {
        return lhs.points[0] == rhs.points[0] && lhs.points[1] == rhs.points[1] && lhs.points[2] == rhs.points[2];
    }
};

