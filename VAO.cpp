#include "VAO.h"
#include <ostream>
#include <iostream>

VAO::VAO(const VBO& vbo, VertexAttributes* attrs)
{
	vbo.bind();

	glGenVertexArrays(1, &ID);
	bind();

	for (int i = 0; i < attrs->size(); i++)
	{
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, attrs->sizes[i], GL_FLOAT, GL_FALSE, attrs->stride, reinterpret_cast<void*>(attrs->offsets[i] * sizeof(float)));
	}

	unbind();
	vbo.unbind();
}

VAO::~VAO()
{
	glDeleteVertexArrays(1, &ID);
}
