#include "Mesh.h"



Mesh::Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Assimp_Texture> textures)
{
  this->vertices = vertices;
  this->indices = indices;
  this->textures = textures;

  // now that we have all the required data, set the vertex buffers and its attribute pointers.
  setupMesh();
  scale(glm::vec3(0.1f, 0.1f, 0.1f));
}


Mesh::~Mesh()
{
}

void Mesh::draw(const GLShader& shader) const
{
  glActiveTexture(GL_TEXTURE0); // active proper texture unit before binding
                                    // retrieve texture number (the N in diffuse_textureN)
  //glUniform1i(glGetUniformLocation(shader.ID, "diffuseTexture"), 0);
  // and finally bind the texture
  glBindTexture(GL_TEXTURE_2D, textures[0].id);

  glActiveTexture(GL_TEXTURE2); // active proper texture unit before binding
                                    // retrieve texture number (the N in diffuse_textureN)
  //glUniform1i(glGetUniformLocation(shader.ID, "normalMap"), 2);
  // and finally bind the texture
  glBindTexture(GL_TEXTURE_2D, textures[2].id);
  shader.setMatrix("model", glm::mat4(1.0f));
  // draw mesh
  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, indices.data());
  glBindVertexArray(0);
}

void Mesh::setupMesh()
{
  // create buffers/arrays
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);
  // load data into vertex buffers
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  // A great thing about structs is that their memory layout is sequential for all its items.
  // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
  // again translates to 3/2 floats which translates to a byte array.
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) * 11, &vertices[0], GL_STATIC_DRAW);

  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), reinterpret_cast<void*>(0));
  // vertex normals
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));
  // vertex texture coords
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), reinterpret_cast<void*>(6 * sizeof(float)));
  // vertex tangent
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), reinterpret_cast<void*>(8 * sizeof(float)));
  
  glBindVertexArray(0);
}
