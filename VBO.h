#pragma once

#include <glad/glad.h>

class VertexAttributes;

class VBO
{
public:
	GLuint ID;
	VBO(const VertexAttributes &attrs);
	~VBO();

	inline void bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, ID);
	}

	inline void unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
};

