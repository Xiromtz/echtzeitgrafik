// EZGRProjekt.cpp : Defines the entry point for the console application.
//

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Renderer.h"
#include "Camera.h"
#include "CameraRide.h"
#include "DriverAPI.h"

Renderer* renderer;
CameraRide* cameraRide;

bool lightingToggled = false;
bool isAtNextAAMode = false;
bool userExit = false;
bool reset = false;

GLFWwindow* setupWindow();
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void processInput(GLFWwindow* window);
void loop(GLFWwindow* window);
DriverAPI AA_API;

int main()
{
  while (!userExit)
  {
    reset = false;
    glfwInit();
    GLFWwindow* window = setupWindow();
    if (window == nullptr)
      return -1;

    loop(window);

    glfwTerminate();
    delete renderer;
    delete cameraRide;
  }
	return 0;
}

GLFWwindow* setupWindow()
{
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return nullptr;
	}
	glViewport(0, 0, 800, 600);
	glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	return window;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}


void loop(GLFWwindow* window)
{
	renderer = new Renderer();
	//cameraRide = new CameraRide(renderer->camera, renderer->curve);

	double sec_per_update = 0.0166;

	double previous = glfwGetTime();
	double lag = 0.0;
	
	//renderer->camera->lock();

	while (!glfwWindowShouldClose(window))
	{
		double current = glfwGetTime();
		double elapsed = current - previous;
		previous = current;
		lag += elapsed;
		
		processInput(window);
    if (reset)
      break;
 		while (lag >= sec_per_update)
		{
			std::unordered_map<GLShader*, std::vector<GLObject*>> objects = renderer->objects;
			for (auto it = objects.begin(); it != objects.end(); ++it)
			{
				std::vector<GLObject*> current = it->second;
				for (unsigned int i = 0; i < current.size(); i++)
				{
					current[i]->update();
				}
			}
			//cameraRide->update();
			
			renderer->camera->update();
			
			lag -= sec_per_update;
		}
		
		renderer->render(window);
	}

  if (!reset)
    userExit = true;
}

void processInput(GLFWwindow* window)
{	
	renderer->camera->ProcessKeyboard(window);

	if (cameraRide != nullptr)
	{
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
			cameraRide->decreaseSpeed();

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			cameraRide->increaseSpeed();
	}

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		if (!lightingToggled)
		{
			renderer->toggleLighting();
			lightingToggled = true;
		}
	}
	else
	{
		if (lightingToggled)
			lightingToggled = false;
	}

  if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
  {
    if (!isAtNextAAMode)
    {
      AA_API.setNextAAMethod();
      isAtNextAAMode = true;
      reset = true;
    }
  }
  else
  {
    if (isAtNextAAMode)
      isAtNextAAMode = false;
  }

	if (glfwGetKey(window, GLFW_KEY_B))
	{
		renderer->addBumpiness();
	}

	if (glfwGetKey(window, GLFW_KEY_V))
	{
		renderer->removeBumpiness();
	}

	if (glfwGetKey(window, GLFW_KEY_J))
	{
		renderer->moveLightLeft();
	}

	if (glfwGetKey(window, GLFW_KEY_L))
	{
		renderer->moveLightRight();
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	renderer->camera->mouseRotate(xpos, ypos);
}