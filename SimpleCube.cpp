#include "SimpleCube.h"

SimpleCube::SimpleCube()
{
	setup();
	init();
}

void SimpleCube::setup()
{
	vAttrs = new VertexAttributes(vertices, 72 * sizeof(float), indices, sizeof(this->indices));
	vAttrs->set_coordinate_attributes();
}

SimpleCube::~SimpleCube()
{
	delete[] vertices;
    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        delete *it;
    }
}

void SimpleCube::draw(const GLShader& shader) const
{
	vao->bind();
	shader.setMatrix("model", model);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, indices);
	vao->unbind();
}

void SimpleCube::setTriangles()
{
    int vertexWidth = 3;
    for (int i = 0; i < 36;)
    {
        int currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex1(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex1 = model * glm::vec4(vertex1, 1.0f);
        i++;
        currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex2(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex2 = model * glm::vec4(vertex2, 1.0f);
        i++;
        currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex3(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex3 = model * glm::vec4(vertex3, 1.0f);
        i++;

        Triangle* triangle = new Triangle(vertex1, vertex2, vertex3);
        triangles.push_back(triangle);
    }
}


std::vector<Triangle*> SimpleCube::getTriangles() const
{
    return triangles;
}
