#include "Median.h"

Median::Median()
{
}

Median::~Median()
{
}

float Median::calculateMedian(std::vector<float>& arr)
{
    int n = arr.size();

    if (n & 1)
    {
        int medianIndex = n / 2;
        return select(&arr[0], medianIndex, n);
    }
    else
    {
        int medianIndexUpper = n / 2;
        int medianIndexLower = medianIndexUpper - 1;
        return (select(&arr[0], medianIndexUpper, n) + select(&arr[0], medianIndexLower, n)) / 2.0f;
    }
}

int Median::select(float* arr, int k, int size)
{
    unsigned int left = 0, right = size - 1;

    while (true)
    {
        if (right <= left + 1)
        {
            if (right == left + 1 && arr[right] < arr[left])
            {
                swap(arr[left], arr[right]);
            }
            return arr[k];
        }

        int threePartMedian = threePartMedianSwap(arr, left, right);
        int i = left + 1;
        int j = right;

        while (true)
        {
            do i++; while (arr[i] < threePartMedian);
            do j--; while (arr[j] > threePartMedian);
            if (j < i) break;
            swap(arr[i], arr[j]);
        }

        swap(arr[left + 1], arr[j]);

        if (j >= k)
            right = j - 1;
        if (j <= k)
            left = i;
    }
}

int Median::threePartMedianSwap(float* arr, int low, int high)
{
    int mid = (low + high) >> 1;

    if (arr[low] > arr[high])
        swap(arr[low], arr[high]);
    if (arr[mid] > arr[high])
        swap(arr[mid], arr[high]);
    if (arr[low] > arr[mid])
        swap(arr[low], arr[mid]);

    swap(arr[mid], arr[low + 1]);
    return arr[low + 1];
}
