#pragma once
#include <vector>

class Median
{
private:
    static int select(float* arr, int k, int size);
    static int threePartMedianSwap(float* arr, int low, int high);
    static void swap(float &a, float &b)
    {
        int temp = a;
        a = b;
        b = temp;
    }

public:
    static float calculateMedian(std::vector<float>& arr);
    Median();
    ~Median();
};

