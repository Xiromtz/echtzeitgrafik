#pragma once
#include "GLObject.h"
#include "SimpleCube.h"
#include "Texture.h"
#include <glm/gtc/quaternion.hpp>

class Curve : public GLObject
{
private:
    std::vector<Triangle*> triangles;
	std::vector<glm::vec3> interpolatedPoints;
	glm::vec3* interpolatedWayPoints;
	float* interpolatedVertices;
	unsigned int interpolatedVertexCount;
	unsigned int* interpolatedIndices;
	unsigned int interpolatedIndexSize;

	float curveVertices[30] =
	{
		0.0f, 0.2f, 0.0f,
		-0.5f, 0.8f, -2.0f,

		-1.0f, 1.2f, -4.0f,
		-0.3f, 1.7f, -6.0f,

		0.5f, 2.0f, -8.0f,
		1.7f, 4.0f, -12.0f,

		3.0f, 6.0f, -14.6f,
		1.9f, 3.5f, -17.0f,

		0.4f, 1.2f, -18.8f,
		-1.0f, -0.7f, -21.0f
	};

	std::vector<SimpleCube*> wayPointCubes;

public:
	const unsigned int wayPointCount = 10;
	glm::vec3 wayPoints[10] =
	{
		glm::vec3(0.0f, 0.2f, 0.0f),
		glm::vec3(-0.5f, 0.8f, -2.0f),
		glm::vec3(-1.0f, 1.2f, -4.0f),
		glm::vec3(-0.3f, 1.7f, -6.0f),
		glm::vec3(0.5f, 2.0f, -8.0f),
		glm::vec3(1.7f, 4.0f, -12.0f),
		glm::vec3(3.0f, 6.0f, -14.6f),
		glm::vec3(1.9f, 3.5f, -17.0f),
		glm::vec3(0.4f, 1.2f, -18.8f),
		glm::vec3(-1.0f, -0.7f, -21.0f)
	};

	glm::vec3 curveAngles[10] =
	{
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(glm::radians(5.0f), 0.0f, 0.0f),
		glm::vec3(glm::radians(15.0f),0.0f, 0.0f),
		glm::vec3(glm::radians(45.0f), glm::radians(20.0f), glm::radians(-45.0f)),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(glm::radians(10.0f), 0.0f, 0.0f),
		glm::vec3(glm::radians(20.0f), 0.0f, glm::radians(10.0f)),
		glm::vec3(glm::radians(30.0f), 0.0f, 0.0f),
		glm::vec3(glm::radians(40.0f), 0.0f, 0.0f),
		glm::vec3(glm::radians(90.0f), 0.0f, 0.0f)
	};

	Curve();
	~Curve();

	void draw(const GLShader& shader) const override;
    void setTriangles();
    std::vector<Triangle*> getTriangles() const override;
	void update() override;
	glm::vec3 interpolatePosition(const unsigned int index1, const unsigned int index2, const float percent) const;
	std::vector<glm::vec3> getInterpolatedPositions(const unsigned int index1, const unsigned int index2, const float step, float alpha, std::vector<glm::vec3> curve);

	std::vector<glm::quat> getInterpolatedRotations(const unsigned int index1, const unsigned int index2, const float step, float alpha, std::vector<glm::quat> rotations);
	glm::quat interpolateRotation(const unsigned int index1, const unsigned int index2, const float percent) const;
	glm::quat squad(glm::quat q0, glm::quat q1, glm::quat q2, glm::quat q3, const float percent) const;
	glm::quat slerp(glm::quat q1, glm::quat q2, const float percent) const;
	glm::quat intermediate(glm::quat q1, glm::quat q2, glm::quat q3) const;
	glm::quat logQuat(glm::quat q) const;
	glm::quat scaleQuat(glm::quat q, float scale) const;
	glm::quat expQuat(glm::quat q) const;
};

