#pragma once
#include <cstdint>
#include <glm/glm.hpp>
#include <vector>
#include "AABB.h"
#include "Triangle.h"
#include <unordered_map>

class KD_Tree
{
public:
    struct KDNode
    {
        explicit KDNode() {}
        explicit KDNode(std::vector<Triangle*> &triangles) : triangles(triangles)
        {
            axis = 3;
        }
        explicit KDNode(KDNode* frontTree, KDNode* backTree) : frontTree(frontTree), backTree(backTree) {}
        short axis = 0;
        float splitVal = 0;
        std::vector<Triangle*> triangles;
        KDNode* frontTree = nullptr, *backTree = nullptr;
    };
private:
    const int MAX_DEPTH = 1000;
    const int MIN_LEAF_SIZE = 1;
    const float PLANE_THICKNESS_EPSILON = 0.6f;
    
    const enum POINT_TO_PLANE
    {
        POINT_IN_FRONT_OF_PLANE,
        POINT_BEHIND_PLANE,
        POINT_ON_PLANE
    };
    
    const enum TRIANGLE_TO_PLANE
    {
        TRIANGLE_STRADDLING_PLANE,
        TRIANGLE_IN_FRONT_OF_PLANE,
        TRIANGLE_BEHIND_PLANE,
        TRIANGLE_COPLANAR_WITH_PLANE
    };

    std::vector<AABB*> aabbs;
    std::vector<std::vector<Triangle*>> ArrayOfLeafPrimitives;
    
    
    void PickSplittingPlane(KDNode& node, const std::vector<Triangle*>& triangles, int axis);

    int selectNextAxis(const std::vector<Triangle*>& triangles) const;
    
    // Return value specifying whether the triangle lies in front of,
    // behind of, on, or straddles the plane
    int ClassifyTriangleToPlane(const Triangle& triangle, const KDNode& plane) const;
    int ClassifyPointToPlane(const glm::vec3& p, const KDNode& plane) const;
public:

     KD_Tree();
     ~KD_Tree();

     int maxDepth = 0;
     void draw(const GLShader& shader) const;
     Triangle* VisitNodes(KDNode* pNode, glm::vec3 p, glm::vec3 d, float tMax);
     KDNode* BuildTree(std::vector<Triangle*> &triangles, const AABB& boundingBox, int depth, float axisVal);
};

