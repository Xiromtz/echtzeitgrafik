#include "EBO.h"

EBO::EBO(const VertexAttributes &attrs, const VAO& vao, const VBO& vbo)
{
	glGenBuffers(1, &ID);
	vbo.bind();
	vao.bind();
	bind();

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, attrs.indexArraySize, attrs.indices, GL_STATIC_DRAW);

	unbind();
	vao.unbind();
	vbo.unbind();
}

EBO::~EBO()
{
	glDeleteBuffers(1, &ID);
}
