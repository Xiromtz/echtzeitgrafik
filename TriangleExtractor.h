#pragma once

#include <vector>
#include <glm\glm.hpp>
#include "Triangle.h"
#include "Mesh.h"
#include "Model.h"

class TriangleExtractor
{
public:
  TriangleExtractor();
  TriangleExtractor(Mesh const& mesh);
  TriangleExtractor(Model const& model);
  virtual ~TriangleExtractor();
  std::vector<Triangle> operator()(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices);
  std::vector<Triangle> operator()();

private:
  bool m_IndicesExistent;
  std::vector<glm::vec3> m_Vertices;
  std::vector<unsigned int> m_Indices;

};

