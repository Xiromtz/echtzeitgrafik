#include "VertexAttributes.h"

VertexAttributes::~VertexAttributes()
{
}

void VertexAttributes::set_coordinate_attributes()
{
	setStride(3 * sizeof(float));
	add(0, 3);
}
void VertexAttributes::set_coordinate_color_attributes()
{
	set_coordinate_attributes();
	setStride(6 * sizeof(float));
	add(3, 3);
}
void VertexAttributes::set_coordinate_color_texture_attributes()
{
	set_coordinate_color_attributes();
	setStride(8 * sizeof(float));
	add(6, 2);
}

void VertexAttributes::set_coordinate_texture_attributes()
{
	set_coordinate_attributes();
	setStride(5 * sizeof(float));
	add(3, 2);
}

void VertexAttributes::set_coordinate_normal_texture_tangent_attributes()
{
	set_coordinate_color_texture_attributes();
	setStride(11 * sizeof(float));
	add(8, 3);
}
