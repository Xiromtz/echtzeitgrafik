#pragma once

#include <glad/glad.h>
#include "Texture.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "GLShader.h"
class Renderer;

class DepthMap
{
private:
	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

	void setupBuffer();
	void setupShader();

public:
	GLShader* shader;
	Texture* depthMap;
	unsigned int ID;
	DepthMap();
	~DepthMap();

	void bind() const;
	void unbind() const;
};

