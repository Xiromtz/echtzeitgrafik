#include "Triangle.h"

Triangle::Triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
    points[0] = a;
    points[1] = b;
    points[2] = c;
    centerOfMass = calculateCenterOfMass();
    normal = calculateNormal();
    setup();
    init();
}

Triangle::~Triangle()
{
    delete centerOfMassVertexAttrs;
    delete centerOfMassVAO;
    delete centerOfMassVBO;
}

void Triangle::setup()
{
    createVertices();
    vAttrs = new VertexAttributes(vertices, 9 * sizeof(float), indices, sizeof(indices));
    vAttrs->set_coordinate_attributes();

    createCenterOfMassVertices();
    centerOfMassVertexAttrs = new VertexAttributes(centerOfMassVertices, 9 * sizeof(float), centerOfMassIndices, sizeof(centerOfMassIndices));
    centerOfMassVertexAttrs->set_coordinate_attributes();

    centerOfMassVBO = new VBO(*centerOfMassVertexAttrs);
    centerOfMassVAO = new VAO(*centerOfMassVBO, centerOfMassVertexAttrs);
}

void Triangle::createVertices()
{
    vertices[0] = points[0].x;
    vertices[1] = points[0].y;
    vertices[2] = points[0].z;

    vertices[3] = points[1].x;
    vertices[4] = points[1].y;
    vertices[5] = points[1].z;

    vertices[6] = points[2].x;
    vertices[7] = points[2].y;
    vertices[8] = points[2].z;
}

void Triangle::createCenterOfMassVertices()
{
    centerOfMassVertices[0] = centerOfMass.x;
    centerOfMassVertices[1] = centerOfMass.y;
    centerOfMassVertices[2] = centerOfMass.z;

    glm::vec3 normalPoint = centerOfMass + normal;
    centerOfMassVertices[3] = normalPoint.x;
    centerOfMassVertices[4] = normalPoint.y;
    centerOfMassVertices[5] = normalPoint.z;
}


glm::vec3 Triangle::calculateCenterOfMass() const
{
    glm::vec3 centerOfMass = (points[0] + points[1] + points[2]) / 3.0f;
    return centerOfMass;
}

glm::vec3 Triangle::calculateNormal() const
{
    glm::vec3 ab = points[1] - points[0];
    glm::vec3 ac = points[2] - points[0];
    return glm::normalize(glm::cross(ab, ac));
}

void Triangle::draw(const GLShader& shader) const
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    vao->bind();
    shader.setMatrix("model", model);
    glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, indices);
    vao->unbind();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    centerOfMassVAO->bind();
    glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, centerOfMassIndices);
    centerOfMassVAO->unbind();
}

float Triangle::intersectSegment(glm::vec3 p, glm::vec3 s) const
{
    glm::vec3 ab = points[1] - points[0];
    glm::vec3 ac = points[2] - points[0];

    // Compute denominator d. if d <= 0, segment is parallel to or points away form triangle, so exit early
    float d = glm::dot(s, normal);
    if (d <= 0.0f)
        return -1;

    // Compute intersection t value of seg with plane of triangle.
    // A ray intersects if 0 <= t. Segment intersects if 0 <= t <= 1.
    // Delay dividing by s until intersectio nhas been found to pierce triangle
    glm::vec3 ap = p - points[0];
    float t = glm::dot(ap, normal);
    if (t < 0.0f)
        return -1;

    // Compute barycentric coordinate components and test if within bounds
    glm::vec3 e = glm::cross(s, ap);
    float v = glm::dot(ac, e);
    if (v < 0.0f || v > d)
        return -1;

    float w = -glm::dot(ab, e);
    if (w < 0.0f || v + w > d)
        return -1;

    // Segment/ray intersects triangle. Perform delayed division and
    // compute the last barycentric coordinate component
    float ood = 1.0f / d;
    t *= ood;
    v *= ood;
    w *= ood;
    float u = 1.0f - v - w;
    return t;
}
