#include "TriangleExtractor.h"

TriangleExtractor::TriangleExtractor()
{
}

TriangleExtractor::TriangleExtractor(const Mesh& mesh)
{
  for (auto vertex : mesh.vertices) {
    m_Vertices.push_back(glm::vec3(vertex.posX, vertex.posY, vertex.posZ));
  }
  m_Indices = mesh.indices;
}

TriangleExtractor::TriangleExtractor(const Model& model)
{
    for (auto mesh : model.meshes)
    {
        for (auto vertex : mesh.vertices)
        {
            m_Vertices.push_back(glm::vec3(vertex.posX, vertex.posY, vertex.posZ));
        }
        m_Indices = mesh.indices;
    }
}


TriangleExtractor::~TriangleExtractor()
{
}

std::vector<Triangle> TriangleExtractor::operator()()
{
  std::vector<Triangle> triangles;
  for (int i = 0; i < m_Indices.size(); i += 3) {
    triangles.push_back(Triangle(m_Vertices[m_Indices[i] - 1], m_Vertices[m_Indices[i + 1] - 1], m_Vertices[m_Indices[i + 2] - 1]));
  }
  return triangles;
}

std::vector<Triangle> TriangleExtractor::operator()(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices)
{
  std::vector<Triangle> triangles;
  for (int i = 0; i < indices.size(); i += 3) {
    triangles.push_back(Triangle(vertices[indices[i] - 1], vertices[indices[i + 1] - 1], vertices[indices[i + 2] - 1]));
  }
  return triangles;
}
