#ifndef MESH_H
#define MESH_H

#include <glad/glad.h> // holds all OpenGL type declarations

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "GLShader.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include "GLObject.h"
#include "Triangle.h"
using namespace std;

struct Vertex {
  // position
    float posX, posY, posZ;
        // normal
    float normX, normY, normZ;
  // texCoords
    float texU, texV;
  // tangent
    float tangX, tangY, tangZ;
};

struct Assimp_Texture {
  unsigned int id;
  string type;
  string path;
};

class Mesh : public GLObject
{
public:
  /*  Mesh Data  */
  vector<Vertex> vertices;
  vector<unsigned int> indices;
  vector<Assimp_Texture> textures;
  unsigned int VAO;
  // constructor
  Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Assimp_Texture> textures);
  ~Mesh();
  // render the mesh
  void draw(const GLShader& shader) const override; 
  std::vector<Triangle*> getTriangles() const override { return std::vector<Triangle*>(); };
private:
    /*  Render data  */
    unsigned int VBO;

    /*  Functions    */
    // initializes all the buffer objects/arrays
    void setupMesh();
};

#endif