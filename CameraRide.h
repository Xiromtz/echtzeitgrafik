#pragma once
#include "Camera.h"
#include "Curve.h"


class CameraRide
{
private:
	bool recalculate = true;

	Camera* camera;
	const Curve* curve;

	unsigned int startIndex;
	unsigned int endIndex;
	unsigned int currentIndex;

	float currentDistance = 0;

	float velocity = 0.8f;
	double time = 0.0f;
	double deltaTime = 0.0f;
	double startTime = 0.0f;
	float alpha = 0.0f;

	float distance(unsigned int startIndex, unsigned int endIndex) const;
public:
	CameraRide(Camera* camera, const Curve* curve);
	~CameraRide();

	void update();
	void increaseSpeed()
	{
		velocity += 0.01f;
	}
	void decreaseSpeed()
	{
		if ((velocity - 0.01f) > 0)
			velocity -= 0.01f;
	}
};

