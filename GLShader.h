#pragma once
#include <glad/glad.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class GLShader
{
private:
	const char* defaultVertexShaderPath = "Shaders/VertexShader.vert";
	const char* defaultFragmentShaderPath = "Shaders/FragmentShader.frag";
	
	
	GLuint setupShaderProgram(const GLuint vertexShader, const GLuint fragmentShader);
	GLuint LoadVertexShader(const char *path) const;
	GLuint LoadFragmentShader(const char *path) const;
	GLuint LoadShader(const char* path, const GLenum type) const;

	std::string readFile(const char* path) const;
public:
	
	GLuint ID;
	
	GLShader(const char* vertexPath, const char* fragmentPath);
	~GLShader();
	
	inline void use() const
	{
		glUseProgram(ID);
	}

	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setMatrix(const std::string &name, glm::mat4 value) const;
	void setVec3(const std::string &name, glm::vec3 value) const;
};

