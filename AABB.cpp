#include "AABB.h"

AABB::AABB(glm::vec3 min, glm::vec3 max) : min(min), max(max)
{
    calculateCenterExtents();
    calcVertices();
    vAttrs = new VertexAttributes(vertices, 72 * sizeof(float), indices, sizeof(indices));
    vAttrs->set_coordinate_attributes();
    init();
}

AABB::AABB(const std::vector<Triangle*>& triangles)
{
    float minX = FLT_MAX, minY = FLT_MAX, minZ = FLT_MAX;
    float maxX = -FLT_MAX, maxY = -FLT_MAX, maxZ = -FLT_MAX;

    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        Triangle* current = *it;
        for (int i = 0; i < 3; i++)
        {
            glm::vec3 currentPoint = current->points[i];

            if (currentPoint.x < minX)
            {
                min.x = currentPoint.x;
                minX = min.x;
            }
            else if (currentPoint.x > maxX)
            {
                max.x = currentPoint.x;
                maxX = max.x;
            }

            if (currentPoint.y < minY)
            {
                min.y = currentPoint.y;
                minY = min.y;
            }
            else if (currentPoint.y > maxY)
            {
                max.y = currentPoint.y;
                maxY = max.y;
            }

            if (currentPoint.z < minZ)
            {
                min.z = currentPoint.z;
                minZ = min.z;
            }
            else if (currentPoint.z > maxZ)
            {
                max.z = currentPoint.z;
                maxZ = max.z;
            }
        }
    }
    calculateCenterExtents();
    calcVertices();
    vAttrs = new VertexAttributes(vertices, 72 * sizeof(float), indices, sizeof(indices));
    vAttrs->set_coordinate_attributes();
    init();
}

void AABB::draw(const GLShader& shader) const
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    vao->bind();
    shader.setMatrix("model", model);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, indices);
    vao->unbind();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void AABB::calculateCenterExtents()
{
    center = (min + max) * 0.5f;
    extents[0] = (max.x - min.x) * 0.5f;
    extents[1] = (max.y - min.y) * 0.5f;
    extents[2] = (max.z - min.z) * 0.5f;
}

void AABB::calcVertices()
{
    // back face
    // left-bottom
    vertices[0] = min.x;
    vertices[1] = min.y;
    vertices[2] = min.z;

    // right-bottom
    vertices[3] = max.x;
    vertices[4] = min.y;
    vertices[5] = min.z;

    // right-top
    vertices[6] = max.x;
    vertices[7] = max.y;
    vertices[8] = min.z;

    //left-top
    vertices[9] = min.x;
    vertices[10] = max.y;
    vertices[11] = min.z;


    //front face
    // left-bottom
    vertices[12] = min.x;
    vertices[13] = min.y;
    vertices[14] = max.z;

    // right-bottom
    vertices[15] = max.x;
    vertices[16] = min.y;
    vertices[17] = max.z;

    // right-top
    vertices[18] = max.x;
    vertices[19] = max.y;
    vertices[20] = max.z;

    //left-top
    vertices[21] = min.x;
    vertices[22] = max.y;
    vertices[23] = max.z;

    
    //left face
    // front-top
    vertices[24] = min.x;
    vertices[25] = max.y;
    vertices[26] = max.z;

    // back-top
    vertices[27] = min.x;
    vertices[28] = max.y;
    vertices[29] = min.z;

    // back-bottom
    vertices[30] = min.x;
    vertices[31] = min.y;
    vertices[32] = min.z;

    // front-bottom
    vertices[33] = min.x;
    vertices[34] = min.y;
    vertices[35] = max.z;

    //right face
    // front-top
    vertices[36] = max.x;
    vertices[37] = max.y;
    vertices[38] = max.z;

    // back-top
    vertices[39] = max.x;
    vertices[40] = max.y;
    vertices[41] = min.z;

    // back-bottom
    vertices[42] = max.x;
    vertices[43] = min.y;
    vertices[44] = min.z;

    // front-bottom
    vertices[45] = max.x;
    vertices[46] = min.y;
    vertices[47] = max.z;

    //bottom face
    //back-left
    vertices[48] = min.x;
    vertices[49] = min.y;
    vertices[50] = min.z;

    // back-right
    vertices[51] = max.x;
    vertices[52] = min.y;
    vertices[53] = min.z;

    //front-right
    vertices[54] = max.x;
    vertices[55] = min.y;
    vertices[56] = max.z;

    //front-left
    vertices[57] = min.x;
    vertices[58] = min.y;
    vertices[59] = max.z;

    // top face
    //back-left
    vertices[60] = min.x;
    vertices[61] = max.y;
    vertices[62] = min.z;

    // back-right
    vertices[63] = max.x;
    vertices[64] = max.y;
    vertices[65] = min.z;

    //front-right
    vertices[66] = max.x;
    vertices[67] = max.y;
    vertices[68] = max.z;

    //front-left
    vertices[69] = min.x;
    vertices[70] = max.y;
    vertices[71] = max.z;
}

AABB::~AABB()
{

}

void AABB::splitAABB(float val, int axis, AABB& front, AABB& back) const
{
    front.max[axis] = val;
    back.min[axis] = val;
}

int AABB::TestTriangle(const Triangle& triangle) const
{
    float p0, p1, p2, r;

    // Translate triangle as conceptually moving aabb to origin
    glm::vec3 v0 = triangle.points[0] - center;
    glm::vec3 v1 = triangle.points[1] - center;
    glm::vec3 v2 = triangle.points[2] - center;

    // Compute edge vectors for triangle
    glm::vec3 f0 = v1 - v0;
    glm::vec3 f1 = v2 - v1;
    glm::vec3 f2 = v0 - v2;

    // Test axis a00 = (0, -f0z, f0y)
    // p0 = v0 * a00 = v0 * (0, -f0z, f0y) = -v0y*f0z + v0z*f0y = -v0y(v1z - v0z) + v0z(v1y - v0y) = -v0y*v1z + v0z*v1y
    p0 = v0.z * v1.y - v0.y * v1.z;
    // for a00, p0 = p1
    // p2 = v2 * a00 = ..
    p2 = v2.z * (v1.y - v0.y) - v2.y * (v1.z - v0.z);
    r = extents[1] * glm::abs(f0.z) + extents[2] * glm::abs(f0.y);
    if (glm::max(-glm::max(p0, p2), glm::min(p0, p2)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a01 = (0, -f1z, f1y)
    p0 = v0.z*(v2.y - v1.y) - v0.y*(v2.z - v1.z);
    p1 = v1.z * v2.y - v1.y * v2.z;
    // p1 == p2
    r = extents[1] * glm::abs(f1.z) + extents[2] * glm::abs(f1.y);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a02 = (0, -f2z, f2y)
    p0 = v0.y * v2.z - v0.z * v2.y;
    p1 = v1.z * (v0.y - v2.y) - v1.y * (v0.z - v2.z);
    // p2 == p0
    r = extents[1] * glm::abs(f2.z) + extents[2] * glm::abs(f2.y);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; //Axis is a seperating axis

    // Test axis a10 = (f0z, 0, -f0x)
    p0 = v0.x * v1.z - v0.z * v1.x;
    //p1 == p0
    p2 = v2.x * (v1.z - v0.z) - v2.z * (v1.x - v0.x);
    r = extents[0] * glm::abs(f0.z) + extents[2] * glm::abs(f0.x);
    if (glm::max(-glm::max(p0, p2), glm::min(p0, p2)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a11 = (f1z, 0, -f1x)
    p0 = v0.x * (v2.z - v1.z) - v0.z * (v2.x - v1.x);
    p1 = v1.x * v2.z - v1.z * v2.x;
    // p1 == p2
    r = extents[0] * glm::abs(f1.z) + extents[2] * glm::abs(f1.x);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a12 = (f2z, 0, -f2x)
    p0 = v0.z * v2.x - v0.x * v2.z;
    p1 = v1.x * (v0.z - v2.z) - v1.z * (v0.x - v2.x);
    // p2 == p0
    r = extents[0] * glm::abs(f2.z) + extents[2] * glm::abs(f2.x);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a20 = (-f0y, f0x, 0)
    p0 = v0.y * v1.x - v0.x * v1.y;
    //p1 == p0
    p2 = v2.y * (v1.x - v0.x) - v2.x * (v1.y - v0.y);
    r = extents[0] * glm::abs(f0.y) + extents[1] * glm::abs(f0.x);
    if (glm::max(-glm::max(p0, p2), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a21 = (-f1y, f1x, 0)
    p0 = v0.y * (v2.x - v1.x) - v0.x * (v2.y - v1.y);
    p1 = v1.y * v2.x - v1.x * v2.y;
    // p2 == p1
    r = extents[0] * glm::abs(f1.y) + extents[1] * glm::abs(f1.x);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test axis a22 = (-f2y, f2x, 0)
    p0 = v0.x * v2.y - v0.y * v2.x;
    p1 = v1.y * (v0.x - v2.x) - v1.x * (v0.y - v2.y);
    // p2 == p0
    r = extents[0] * glm::abs(f2.y) + extents[1] * glm::abs(f2.x);
    if (glm::max(-glm::max(p0, p1), glm::min(p0, p1)) > r)
        return 0; // Axis is a seperating axis

    // Test the three axes corresponding to the face normals of AABB b
    // Exit if 
    // [-extentX, extentX] and [min(v0.x, v1.x, v2.x), max(v0.x,v1.x,v2.x)] do not overlap
    if (glm::max(v0.x, glm::max(v1.x, v2.x)) < -extents[0] || glm::min(v0.x, glm::min(v1.x, v2.x)) > extents[0])
        return 0;

    // [-extentY, extentY] and [min(v0.y, v1.y, v2.y), max(v0.y,v1.y,v2.y)] do not overlap
    if (glm::max(v0.y, glm::max(v1.y, v2.y)) < -extents[1] || glm::min(v0.y, glm::min(v1.y, v2.y)) > extents[1])
        return 0;

    // [-extentZ, extentZ] and [min(v0.z, v1.z, v2.z), max(v0.z, v1.z, v2.z)] do not overlap
    if (glm::max(v0.z, glm::max(v1.z, v2.z)) < -extents[2] || glm::min(v0.z, glm::min(v1.z, v2.z)) > extents[2])
        return 0;

    // Test separating axis corresponding to triangle face normal
    glm::vec3 planeNormal = glm::cross(f0, f1);
    float planeDistToOrigin = glm::dot(planeNormal, v0);
    return TestPlane(planeNormal, planeDistToOrigin);
}

int AABB::TestPlane(glm::vec3 normal, float dist) const
{
    // Compute the projection interval radius of aabb onto L(t) = center + t * planeNormal
    float r = extents[0] * glm::abs(normal.x) +
        extents[1] * glm::abs(normal.y) +
        extents[2] * glm::abs(normal.z);
    
    // Compute distance of box center from plane
    float s = glm::dot(normal, center) - dist;
    // Intersection occurs when distance s falls within [-r, +r] interval
    return glm::abs(s) <= r;
}
