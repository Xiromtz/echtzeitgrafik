#include "Texture.h"

Texture::Texture(const char* path, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type) : textureNum(textureNum)
{
	loadImage(path);
	generateTexture(s_wrap, t_wrap, minFilter, magFilter, format, type);
}

Texture::Texture(int width, int height, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type) : textureNum(textureNum)
{
	this->width = width;
	this->height = height;
	generateTexture(s_wrap, t_wrap, minFilter, magFilter, format, type);
}

void Texture::loadImage(const char* path)
{
	if (path == nullptr)
	{
		data = nullptr;
		return;
	}
	stbi_set_flip_vertically_on_load(true);
	data = stbi_load(path, &width, &height, &nrChannels, 0);
}

void Texture::generateTexture(GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum format, GLenum type)
{
	
	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_2D, ID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s_wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t_wrap);

	if (s_wrap == GL_CLAMP_TO_BORDER || t_wrap == GL_CLAMP_TO_BORDER)
	{
		float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, type, data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	
	if (data != nullptr)
		stbi_image_free(data);
}


Texture::~Texture()
{
}
