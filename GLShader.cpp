#include "GLShader.h"


GLShader::GLShader(const char* vertexPath, const char* fragmentPath)
{
	GLuint vertexShader = LoadVertexShader(vertexPath);
	if (vertexShader == 0)
		return;

	GLuint fragmentShader = LoadFragmentShader(fragmentPath);
	if (fragmentShader == 0)
		return;

	ID = setupShaderProgram(vertexShader, fragmentShader);
	if (ID == 0)
		return;

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

GLuint GLShader::setupShaderProgram(const GLuint vertexShader, const GLuint fragmentShader)
{
	const GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	int success;
	char infolog[512];
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, nullptr, infolog);
		std::cout << "ERROR::SHADER::PROGRAM::COMPILATION::FAILED\n" << infolog << std::endl;
		return NULL;
	}

	return shaderProgram;
}


GLuint GLShader::LoadVertexShader(const char* path) const
{
	return LoadShader(path, GL_VERTEX_SHADER);
}

GLuint GLShader::LoadFragmentShader(const char* path) const
{
	return LoadShader(path, GL_FRAGMENT_SHADER);
}

GLuint GLShader::LoadShader(const char* path, const GLenum type) const
{
	const GLuint shader = glCreateShader(type);
	std::string shaderString = readFile(path);

	if (shaderString.size() <= 0)
		return NULL;

	const char* shader_cString = shaderString.c_str();
	glShaderSource(shader, 1, &(shader_cString), nullptr);
	glCompileShader(shader);

	int success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, nullptr, infoLog);
		std::cout << "ERROR:SHADER::VERTEX::COMPILATION::FAILED\n" << infoLog << std::endl;
		return NULL;
	}
	return shader;
}


std::string GLShader::readFile(const char* path) const
{
	std::string code;
	std::ifstream file;

	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		file.open(path);
		std::stringstream shaderStream;
		shaderStream << file.rdbuf();
		file.close();
		code = shaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY READ: " << e.what() << std::endl;
	}

	return code;
}

void GLShader::setBool(const std::string& name, bool value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), static_cast<int>(value));
}

void GLShader::setInt(const std::string& name, int value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

void GLShader::setFloat(const std::string& name, float value) const
{
	glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void GLShader::setMatrix(const std::string& name, glm::mat4 value) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

void GLShader::setVec3(const std::string& name, glm::vec3 value) const
{
	glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, glm::value_ptr(value));
}


GLShader::~GLShader()
{
}
