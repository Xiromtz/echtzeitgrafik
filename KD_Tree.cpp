#include "KD_Tree.h"
#include <cassert>
#include "Median.h"
#include <algorithm>

KD_Tree::KD_Tree()
{
}

KD_Tree::~KD_Tree()
{
    for (auto it = aabbs.begin(); it != aabbs.end(); ++it)
    {
        delete *it;
    }
}

void KD_Tree::draw(const GLShader& shader) const
{
    for (auto it = aabbs.begin(); it != aabbs.end(); ++it)
    {
        (*it)->draw(shader);
    }
}


KD_Tree::KDNode* KD_Tree::BuildTree(std::vector<Triangle*>& triangles, const AABB& boundingBox, int depth, float axisVal)
{
    if (triangles.empty())
        return nullptr;

    int numTriangles = triangles.size();

    if (depth > maxDepth)
        maxDepth = depth;

    bool onlyCoplanar = false;
    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        if (!(*it)->ignore)
        {
            onlyCoplanar = false;
            break;
        }
        onlyCoplanar = true;
    }
    // If criteria for a leaf is matched, create a leaf node from remaining polygons
    if (depth >= MAX_DEPTH || numTriangles <= MIN_LEAF_SIZE || onlyCoplanar)
    {
        AABB* leaf = new AABB(triangles);
        aabbs.push_back(leaf);

        return new KDNode(triangles);
    }

    int axis = selectNextAxis(triangles);
    
    // Calculate best splitting plane (median)
    KDNode* splitNode = new KDNode();
    PickSplittingPlane(*splitNode, triangles, axis);

    if (splitNode->splitVal == axisVal)
    {
        AABB* leaf = new AABB(triangles);
        aabbs.push_back(leaf);

        return new KDNode(triangles);
    }
    
    AABB* front = new AABB(boundingBox.min, boundingBox.max), *back = new AABB(boundingBox.min, boundingBox.max);
    boundingBox.splitAABB(splitNode->splitVal, axis, *front, *back);
    aabbs.push_back(front);
    aabbs.push_back(back);

    std::vector<Triangle*> frontList, backList;

    // Test each polygon against the dividing plane, adding them
    // to the front list, back list, or both, as appropriate
    for (int i = 0; i < numTriangles; i++)
    {
        Triangle* triangle = triangles[i];
        switch (ClassifyTriangleToPlane(*triangle, *splitNode))
        {
        case TRIANGLE_COPLANAR_WITH_PLANE:
            // leaf-storing tree: coplanar polygons are sent to the front side
            triangle->ignore = true;
        case TRIANGLE_IN_FRONT_OF_PLANE:
            frontList.push_back(triangle);
            break;
        case TRIANGLE_BEHIND_PLANE:
            backList.push_back(triangle);
            break;
        case TRIANGLE_STRADDLING_PLANE:
            if (front->TestTriangle(*triangle))
                frontList.push_back(triangle);

            if (back->TestTriangle(*triangle))
                backList.push_back(triangle);
            break;
        }
    }

    splitNode->frontTree = BuildTree(frontList, *front, depth + 1, splitNode->splitVal);
    splitNode->backTree = BuildTree(backList, *back, depth + 1, splitNode->splitVal);
    return splitNode;
}

void KD_Tree::PickSplittingPlane(KDNode& node, const std::vector<Triangle*>& triangles, int axis)
{
    std::vector<float> axisVals;
    for (int i = 0; i < triangles.size(); i++)
    {
        if (!triangles[i]->ignore)
        {
            glm::vec3 curCenter = triangles[i]->centerOfMass;
            axisVals.push_back(curCenter[axis]);
        }
    }
    //float median = Median::calculateMedian(axisVals);
    int n = axisVals.size();
    float median = 0;
    if (n & 1)
    {
        int medianIndex = n / 2;
        std::nth_element(axisVals.begin(), axisVals.begin() + medianIndex, axisVals.end());
        median = axisVals[medianIndex];
    }
    else
    {
        int medianIndexUpper = n / 2;
        int medianIndexLower = medianIndexUpper - 1;

        std::nth_element(axisVals.begin(), axisVals.begin() + medianIndexUpper, axisVals.end());
        float upperMedian = axisVals[medianIndexUpper];

        std::nth_element(axisVals.begin(), axisVals.begin() + medianIndexLower, axisVals.end());
        float lowerMedian = axisVals[medianIndexLower];

        median = (upperMedian + lowerMedian) / 2.0f;
    }
    node.axis = axis;
    node.splitVal = median;
}

int KD_Tree::ClassifyTriangleToPlane(const Triangle& triangle, const KDNode& plane) const
{
    // Loop over all triangle vertices and count how many vertices
    // lie in front of and how many lie behind of the thickened plane
    int numInFront = 0, numBehind = 0;
    int numVerts = 3;
    for (int i = 0; i < numVerts; i++)
    {
        glm::vec3 point = triangle.points[i];
        switch (ClassifyPointToPlane(point, plane))
        {
        case POINT_IN_FRONT_OF_PLANE:
            numInFront++;
            break;
        case POINT_BEHIND_PLANE:
            numBehind++;
            break;
        }
    }

    // If vertices on both side of the plane, the triangle is straddling
    if (numBehind != 0 && numInFront != 0)
        return TRIANGLE_STRADDLING_PLANE;

    // If one or more vertices in front of the plane and no vertices behind the plane,
    // the polygon lies in front of the plane
    if (numInFront != 0)
        return TRIANGLE_IN_FRONT_OF_PLANE;

    // Ditto, the polygon lies behind the plane if no vertices in front of the plane,
    // and one or more vertices behind the plane
    if (numBehind != 0)
        return TRIANGLE_BEHIND_PLANE;

    // All vertices lie on the plane so the triangle is coplanar with the plane
    return TRIANGLE_COPLANAR_WITH_PLANE;
}

int KD_Tree::ClassifyPointToPlane(const glm::vec3& p, const KDNode& plane) const
{
    int axis = plane.axis;
    glm::vec3 normal(0.0f);
    normal[axis] = 1.0f;
    
    glm::vec3 pointOnPlane(0.0f);
    pointOnPlane[axis] = plane.splitVal;

    // Distance plane to origin
    float distToOrigin = glm::dot(normal, pointOnPlane);
    // Compute signed distance of point from plane
    float dist = glm::dot(normal, p) - distToOrigin;

    if (dist > PLANE_THICKNESS_EPSILON)
        return POINT_IN_FRONT_OF_PLANE;
    if (dist < -PLANE_THICKNESS_EPSILON)
        return POINT_BEHIND_PLANE;

    return POINT_ON_PLANE;
}

int KD_Tree::selectNextAxis(const std::vector<Triangle*>& triangles) const
{
    float minX = FLT_MAX, minY = FLT_MAX, minZ = FLT_MAX;
    float maxX = -FLT_MAX, maxY = -FLT_MAX, maxZ = -FLT_MAX;

    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        Triangle* current = *it;
        for (int i = 0; i < 3; i++)
        {
            glm::vec3 currentPoint = current->points[i];

            if (currentPoint.x < minX)
            {
                minX = currentPoint.x;
            }
            else if (currentPoint.x > maxX)
            {
                maxX = currentPoint.x;
            }

            if (currentPoint.y < minY)
            {
                minY = currentPoint.y;
            }
            else if (currentPoint.y > maxY)
            {
                maxY = currentPoint.y;
            }

            if (currentPoint.z < minZ)
            {
                minZ = currentPoint.z;
            }
            else if (currentPoint.z > maxZ)
            {
                maxZ = currentPoint.z;
            }
        }
    }

    float diffX = glm::abs(maxX - minX);
    float diffY = glm::abs(maxY - minY);
    float diffZ = glm::abs(maxZ - minZ);

    return diffX > diffY ? (diffX > diffZ ? 0 : 2) : (diffY > diffZ ? 1 : 2);
}


// VIsit all k-d tree nodes intersected by segment S = a + t*d, 0 <= t < tMax
Triangle* KD_Tree::VisitNodes(KDNode* pNode, glm::vec3 a, glm::vec3 d, float tMax)
{
    if (pNode == nullptr)
        return nullptr;

    // Get split-dimension
    int dim = pNode->axis;
    Triangle* triangle = nullptr;
    // If not leaf node
    if (dim < 3)
    {
        //Figure out which child to recurse into first (false = near, true = far)
        int first = a[dim] > pNode->splitVal;

        if (d[dim] == 0.0f)
        {
            // Segment parallel to splitting plane, visit near side only
            triangle = VisitNodes(first ? pNode->frontTree : pNode->backTree, a, d, tMax);
            if (triangle != nullptr)
                return triangle;
        }
        else
        {
            // Find t value for intersection between segment and split plane
            float t = (pNode->splitVal - a[dim]) / d[dim];

            // Test if line segment straddles splitting plane
            if (0.0f <= t && t < tMax)
            {
                // Yes, traverse near side first, then far side
                triangle = VisitNodes(first ? pNode->frontTree : pNode->backTree, a, d, t);
                if (triangle != nullptr)
                    return triangle;
                triangle = VisitNodes(first ? pNode->backTree : pNode->frontTree, a + t * d, d, tMax - t);
                if (triangle != nullptr)
                    return triangle;
            }
            else
            {
                // No, so just traverse near side
                triangle = VisitNodes(first ? pNode->frontTree : pNode->backTree, a, d, tMax);
            }
        }
    }
    else
    {
        float maxDist = -1;
        float dist = -1;
        for (auto it = pNode->triangles.begin(); it != pNode->triangles.end(); ++it)
        {
            dist = (*it)->intersectSegment(a, d);
            if (dist > maxDist)
            {
                triangle = *it;
                maxDist = dist;
            }
        }
        if (triangle != nullptr)
            return triangle;
    }
    return triangle;
}