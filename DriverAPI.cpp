#include "DriverAPI.h"
#include <NVAPI/nvapi.h>

DriverAPI::DriverAPI()
{
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_NONE);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_6X_CT);

  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_4X_SKEWED_4TAP);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_6X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_6X_SKEWED_6TAP);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_8X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_8X_SKEWED_8TAP);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_16X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_32X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_64X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MIXEDSAMPLE_128X);
  
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_2X_QUINCUNX);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_2X_DIAGONAL);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_2X_DIAGONAL_FOSGAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_2X_DIAGONAL_GAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_4X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_4X_FOSGAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_4X_GAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_4X_GAUSSIAN);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_8X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_MULTISAMPLE_16X);

  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_1_5X1_5);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_2X_H);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_2X_V);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_4X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_4X_BIAS);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_4X_FOSGAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_4X_GAMMA);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_4X_GAUSSIAN);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_9X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_9X_BIAS);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_16X);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERSAMPLE_16X_BIAS);

  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_VCAA_8X_4v4);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_VCAA_16X_8v8);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_VCAA_16X_4v12);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_VCAA_32X_8v24);
  
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERVCAA_64X_8v8);
  aaMethods.push_back(EValues_AA_MODE_METHOD::AA_MODE_METHOD_SUPERVCAA_64X_4v12);

  NvAPI_Status status;
  status = NvAPI_Initialize();
  if (status != NVAPI_OK)
    PrintError(status);

  hSession = 0;
  status = NvAPI_DRS_CreateSession(&hSession);
  if (status != NVAPI_OK)
    PrintError(status);

  status = NvAPI_DRS_LoadSettings(hSession);
  if (status != NVAPI_OK)
    PrintError(status);

  hProfile = 0;
  status = NvAPI_DRS_GetCurrentGlobalProfile(hSession, &hProfile);
  if (status != NVAPI_OK)
    PrintError(status);

  NvAPI_UnicodeString antialiasingMode;
  memcpy_s(antialiasingMode, sizeof(antialiasingMode), L"Antialiasing - Setting", 23*sizeof(wchar_t));
  status = NvAPI_DRS_GetSettingIdFromName(antialiasingMode, &antialiasingModeID);
  if (status != NVAPI_OK)
    PrintError(status);

  antiAliasingSetting = { 0 };
  antiAliasingSetting.version = NVDRS_SETTING_VER;
  antiAliasingSetting.settingType = NVDRS_DWORD_TYPE;
  antiAliasingSetting.settingId = antialiasingModeID;
}

DriverAPI::~DriverAPI()
{
  antiAliasingSetting.u32CurrentValue = EValues_AA_MODE_METHOD::AA_MODE_METHOD_NONE;
  NvAPI_DRS_SetSetting(hSession, hProfile, &antiAliasingSetting);
  NvAPI_DRS_SaveSettings(hSession);
  NvAPI_DRS_DestroySession(hSession);
  hSession = 0;
}

void DriverAPI::setNextAAMethod()
{
  NvAPI_Status status;
  aaIndex = (aaIndex + 1)%aaMethods.size();
  std::cout << "Current index: " << aaIndex << std::endl;

  antiAliasingSetting.u32CurrentValue = aaMethods[aaIndex];
  status = NvAPI_DRS_SetSetting(hSession, hProfile, &antiAliasingSetting);
  if (status != NVAPI_OK)
    PrintError(status);

  status = NvAPI_DRS_SaveSettings(hSession);
  if (status != NVAPI_OK)
    PrintError(status);
}


void DriverAPI::PrintError(NvAPI_Status status)
{
  NvAPI_ShortString szDesc = { 0 };
  NvAPI_GetErrorMessage(status, szDesc);
  printf("NVAPI error: %s\n", szDesc);
  exit(-1);
}
