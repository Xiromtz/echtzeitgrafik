#pragma once

#include "stb_image.h"
#include <glad/glad.h>
#include <iostream>

class Texture
{
private:
	int width, height, nrChannels;
	unsigned char* data = nullptr;
	GLenum textureNum;
	
	void loadImage(const char* path);
	void generateTexture(GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum format, GLenum type);
public:

	GLuint ID;
	Texture(const char* path, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type);
	Texture(int width, int height, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type);
	~Texture();
	
	inline void bind() const
	{
		glActiveTexture(textureNum);
		glBindTexture(GL_TEXTURE_2D, ID);
	}

	inline void unbind() const
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
};

